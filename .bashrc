#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Automatically cd
shopt -s autocd

# Dot file git repo alias
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Default URI for QEMU
export LIBVIRT_DEFAULT_URI=qemu:///system

# Make my terminal pretty
alias ls='ls --color=auto'
PS1="\[\033[38;5;2m\][\[$(tput sgr0)\]\[\033[38;5;40m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;2m\]]\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;36m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;34m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

# Package not found helper
source /usr/share/doc/pkgfile/command-not-found.bash

# Auto cd
shopt -s autocd

# Add local and .local to path
export PATH=$PATH:~/.local/bin:/usr/local/bin

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
